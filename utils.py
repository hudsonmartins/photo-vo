import math
import cv2
import torch
import numpy as np
from collections import defaultdict
import torch.nn.functional as F
from scipy.spatial.transform import Rotation


def interpolate_depth(pos, depth):

    device = pos.device
    ids = torch.arange(0, pos.size(1), device=device)
    h, w = depth.size()
    
    i = pos[1, :]
    j = pos[0, :]

    # Valid corners
    i_top_left = torch.floor(i).long()
    j_top_left = torch.floor(j).long()
    valid_top_left = torch.min(i_top_left >= 0, j_top_left >= 0)

    i_top_right = torch.floor(i).long()
    j_top_right = torch.ceil(j).long()
    valid_top_right = torch.min(i_top_right >= 0, j_top_right < w)

    i_bottom_left = torch.ceil(i).long()
    j_bottom_left = torch.floor(j).long()
    valid_bottom_left = torch.min(i_bottom_left < h, j_bottom_left >= 0)

    i_bottom_right = torch.ceil(i).long()
    j_bottom_right = torch.ceil(j).long()
    valid_bottom_right = torch.min(i_bottom_right < h, j_bottom_right < w)

    valid_corners = torch.min(
        torch.min(valid_top_left, valid_top_right),
        torch.min(valid_bottom_left, valid_bottom_right)
    )

    i_top_left = i_top_left[valid_corners]
    j_top_left = j_top_left[valid_corners]

    i_top_right = i_top_right[valid_corners]
    j_top_right = j_top_right[valid_corners]

    i_bottom_left = i_bottom_left[valid_corners]
    j_bottom_left = j_bottom_left[valid_corners]

    i_bottom_right = i_bottom_right[valid_corners]
    j_bottom_right = j_bottom_right[valid_corners]

    ids = ids[valid_corners]

    # Valid depth
    valid_depth = torch.min(
        torch.min(
            depth[i_top_left, j_top_left] > 0,
            depth[i_top_right, j_top_right] > 0
        ),
        torch.min(
            depth[i_bottom_left, j_bottom_left] > 0,
            depth[i_bottom_right, j_bottom_right] > 0
        )
    )

    i_top_left = i_top_left[valid_depth]
    j_top_left = j_top_left[valid_depth]

    i_top_right = i_top_right[valid_depth]
    j_top_right = j_top_right[valid_depth]

    i_bottom_left = i_bottom_left[valid_depth]
    j_bottom_left = j_bottom_left[valid_depth]

    i_bottom_right = i_bottom_right[valid_depth]
    j_bottom_right = j_bottom_right[valid_depth]

    ids = ids[valid_depth]

    # Interpolation
    i = i[ids]
    j = j[ids]
    dist_i_top_left = i - i_top_left.float()
    dist_j_top_left = j - j_top_left.float()
    w_top_left = (1 - dist_i_top_left) * (1 - dist_j_top_left)
    w_top_right = (1 - dist_i_top_left) * dist_j_top_left
    w_bottom_left = dist_i_top_left * (1 - dist_j_top_left)
    w_bottom_right = dist_i_top_left * dist_j_top_left
    
    interpolated_depth = (
        w_top_left * depth[i_top_left, j_top_left] +
        w_top_right * depth[i_top_right, j_top_right] +
        w_bottom_left * depth[i_bottom_left, j_bottom_left] +
        w_bottom_right * depth[i_bottom_right, j_bottom_right]
    )

    pos = torch.cat([j.view(1, -1), i.view(1, -1)], dim=0)

    return [interpolated_depth, pos, ids]


def preprocess_image(image, preprocessing=None):
    image = image.astype(np.float32)
    image = np.moveaxis(image, -1, 0)
    #image = np.expand_dims(image, 0)
    if preprocessing == 'caffe':
        # RGB -> BGR
        image = image[:: -1, :, :]
        # Zero-center by mean pixel
        mean = np.array([103.939, 116.779, 123.68])
        image = image - mean.reshape([3, 1, 1])
    else:
        pass
    return image


def create_kpts_image(img, kpts, color=(255,255,255)):
    for k in kpts:
        img = cv2.circle(img, (int(k[0]), int(k[1])), 3, color, 2)
    return img


def pad_data(data, max_kpts, img_shape, device):
    _, _, width, _ = img_shape

    for k in data:
        if isinstance(data[k], (list, tuple)):
            new_data = []
            if(k.startswith('keypoints')):
                #padding keypoints
                for kpt in data[k]:
                    #random_values = torch.Tensor(max_kpts - kpt.shape[0], 2).uniform_(0, width)
                    random_values = torch.randint(0, width, (max_kpts - kpt.shape[0], 2))
                    new_data += [torch.cat((kpt, random_values.to(device)), 0)]
                    
            if(k.startswith('descriptor')):
                #padding descriptors
                for desc in data[k]:
                    new_data += [F.pad(desc, 
                                (0, max_kpts - desc.shape[1]))]

            if(k.startswith('score')):
                #padding scores
                for score in data[k]:
                    new_data += [F.pad(score, 
                                (0, max_kpts - score.shape[0]))]
            data[k] = torch.stack(new_data)
    return data
    

def replace_ignored(data, ignore, img_shape, device):
    _, _, width, _ = img_shape

    for img_id in ['0', '1']:
        for k in data:
            batch_size = data[k].size(0)
            if(k.startswith('keypoints'+img_id)):
                for i in range(batch_size):
                    for id in ignore['ignored'+img_id][i]:
                        new_row = torch.randint(0, width, (1, 2))
                        data[k][i][id] = new_row
            if(k.startswith('score'+img_id)):
                for i in range(batch_size):        
                    for id in ignore['ignored'+img_id][i]:
                        data[k][i][id] = 0
    return data


def arange_like(x, dim: int):
    return x.new_ones(x.shape[dim]).cumsum(0) - 1  # traceable in 1.1


def min_row_col(tensor):
    i = 0
    smallest, min_i, min_j = None, None, None
    for row in tensor:
        min_value = torch.min(row)
        if(smallest == None or min_value < smallest):
            smallest = min_value
            min_i = i
            min_j = torch.argmin(row).item()
        i += 1

    return min_i, min_j


def draw_patches(img, patches_corners, color=(0,0,0), thickness=2):
    for corner in patches_corners:
        img = cv2.rectangle(img, (int(corner[0][0]), int(corner[0][1])), 
                                 (int(corner[1][0]), int(corner[1][1])), color, thickness)
    return img


def draw_pts(img, pts, color=(255, 255, 255), radius=0, thickness=-1):
    for pt in pts:
        img = cv2.circle(img, (int(pt[0]), int(pt[1])), radius=radius, color=color, thickness=thickness)
    return img


def patch_meshgrid(x_min, x_max, y_min, y_max):
    xs = torch.range(x_min, x_max-1, step=1)
    ys = torch.range(y_min, y_max-1, step=1)
    gx, gy = torch.meshgrid(xs, ys)
    grid = torch.cat((torch.unsqueeze(gx, dim=2), torch.unsqueeze(gy, dim=2)), dim=2)
    return grid


def get_only_balanced(data, gt, max_kpts):
    new_data = defaultdict(lambda: None)
    new_gt = None
    for i in range(gt.size(0)):
        valid_ids = (gt[i] != -1).nonzero(as_tuple=True)
        filtered_target = gt[i][valid_ids]
        pos_ids = (filtered_target < max_kpts).nonzero(as_tuple=True)
        neg_ids = (filtered_target == max_kpts).nonzero(as_tuple=True) 
        total_size = len(pos_ids[0])+len(neg_ids[0])
        
        if(len(pos_ids[0])/total_size > 0.5):
            if(new_gt == None):
                new_gt = torch.unsqueeze(gt[i], dim=0)
            else:
                new_gt = torch.cat((new_gt, torch.unsqueeze(gt[i], dim=0)), dim=0)
            
            for k in data:
                if(new_data[k] == None):
                    new_data[k] = torch.unsqueeze(data[k][i], dim=0)
                else:
                    new_data[k] = torch.cat((new_data[k], torch.unsqueeze(data[k][i], dim=0)), dim=0)
    return new_data, new_gt


def fill_dustbins(matches):
    rows = torch.count_nonzero(matches, dim=1)
    cols = torch.count_nonzero(matches, dim=0)
    dust_col = rows.clone()
    dust_row = cols.clone()
    dust_col[rows == 0] = 1
    dust_col[rows != 0] = 0
    dust_row[cols == 0] = 1
    dust_row[cols != 0] = 0
    matches[:,-1] = dust_col
    matches[-1,:] = dust_row
    return matches


def ohe_to_le(ohe_tensor):
    '''
        Function to convert one hot encoding to label encoding. Notice that if all elements in a row/cols are zero, the keypoint has no match, 
        thus its label is assigned to n_rows/n_cols. MOreover, if the keypoint is ignored its label is assigned to -1
    '''
    le_tensor = torch.full((ohe_tensor.size(0), ohe_tensor.size(-1)), ohe_tensor.size(-1))
    match_ids = (ohe_tensor == 1).nonzero(as_tuple=True)
    ignored_ids = (ohe_tensor == -1).nonzero(as_tuple=True)    
    le_tensor[match_ids[:2]] = match_ids[2]
    le_tensor[ignored_ids[:2]] = -1
    
    return le_tensor


def get_kpts_depths(kpts_batch, depth_batch, device):
    keypoint_depth_batch = torch.full((kpts_batch.size(0), kpts_batch.size(1)), 
                                        -1, device=device).float()
    for idx_in_batch in range(len(kpts_batch)):
        kpts = kpts_batch[idx_in_batch].to(device)
        kpts = torch.transpose(kpts, 0, 1)
        depths, _, ids = interpolate_depth(kpts, depth_batch[idx_in_batch].to(device))
        keypoint_depth_batch[idx_in_batch, ids] = depths/torch.max(depths)
    return keypoint_depth_batch


def save_model(path, model, optimizer, step, epoch, loss):
    torch.save({'epoch': epoch,
                'step': step,
                'kenc': model.kenc.state_dict(),
                'penc': model.penc.state_dict(),
                'gnn': model.gnn.state_dict(),
                'final_proj': model.final_proj.state_dict(),
                'optimizer': optimizer.state_dict(),
                'loss': loss}, 
                path)
    print(f'Model {path} saved!')


def load_model_weights(model, path, recover_state=False, modules=['gnn', 'final_proj']):
    print('Loading model ', path)
    ckpt = torch.load(str(path))
    if('kenc' in modules):
        model.kenc.load_state_dict(ckpt['kenc'])
    if('penc' in modules):
        model.penc.load_state_dict(ckpt['penc'])
    if('gnn' in modules):
        model.gnn.load_state_dict(ckpt['gnn'])
    if('final_proj' in modules):
        model.final_proj.load_state_dict(ckpt['final_proj'])
    if(recover_state):
        return model, ckpt['epoch'], ckpt['step'], ckpt['optimizer'], ckpt['loss']
    return model

	
def euler_from_matrix(matrix):
    R = Rotation.from_matrix(matrix)
    z,y,x = R.as_euler('zyx')
    return np.array([x,y,z])

"""
def bilinear_sampler(imgs, coords):
 
  def _repeat(x, n_repeats):
    rep = tf.transpose(
        tf.expand_dims(tf.ones(shape=tf.stack([
            n_repeats,
        ])), 1), [1, 0])
    rep = tf.cast(rep, 'float32')
    x = tf.matmul(tf.reshape(x, (-1, 1)), rep)
    return tf.reshape(x, [-1])

  with tf.name_scope('image_sampling'):
    coords_x, coords_y = tf.split(coords, [1, 1], axis=3)
    inp_size = imgs.get_shape()
    coord_size = coords.get_shape()
    out_size = coords.get_shape().as_list()
    out_size[3] = imgs.get_shape().as_list()[3]

    coords_x = tf.cast(coords_x, 'float32')
    coords_y = tf.cast(coords_y, 'float32')

    x0 = tf.floor(coords_x)
    x1 = x0 + 1
    y0 = tf.floor(coords_y)
    y1 = y0 + 1

    y_max = tf.cast(tf.shape(imgs)[1] - 1, 'float32')
    x_max = tf.cast(tf.shape(imgs)[2] - 1, 'float32')
    zero = tf.zeros([1], dtype='float32')

    x0_safe = tf.clip_by_value(x0, zero, x_max)
    y0_safe = tf.clip_by_value(y0, zero, y_max)
    x1_safe = tf.clip_by_value(x1, zero, x_max)
    y1_safe = tf.clip_by_value(y1, zero, y_max)

    ## bilinear interp weights, with points outside the grid having weight 0
    # wt_x0 = (x1 - coords_x) * tf.cast(tf.equal(x0, x0_safe), 'float32')
    # wt_x1 = (coords_x - x0) * tf.cast(tf.equal(x1, x1_safe), 'float32')
    # wt_y0 = (y1 - coords_y) * tf.cast(tf.equal(y0, y0_safe), 'float32')
    # wt_y1 = (coords_y - y0) * tf.cast(tf.equal(y1, y1_safe), 'float32')

    wt_x0 = x1_safe - coords_x
    wt_x1 = coords_x - x0_safe
    wt_y0 = y1_safe - coords_y
    wt_y1 = coords_y - y0_safe

    ## indices in the flat image to sample from
    dim2 = tf.cast(inp_size[2], 'float32')
    dim1 = tf.cast(inp_size[2] * inp_size[1], 'float32')
    base = tf.reshape(
        _repeat(
            tf.cast(tf.range(coord_size[0]), 'float32') * dim1,
            coord_size[1] * coord_size[2]),
        [out_size[0], out_size[1], out_size[2], 1])

    base_y0 = base + y0_safe * dim2
    base_y1 = base + y1_safe * dim2
    idx00 = tf.reshape(x0_safe + base_y0, [-1])
    idx01 = x0_safe + base_y1
    idx10 = x1_safe + base_y0
    idx11 = x1_safe + base_y1

    ## sample from imgs
    imgs_flat = tf.reshape(imgs, tf.stack([-1, inp_size[3]]))
    imgs_flat = tf.cast(imgs_flat, 'float32')
    im00 = tf.reshape(tf.gather(imgs_flat, tf.cast(idx00, 'int32')), out_size)
    im01 = tf.reshape(tf.gather(imgs_flat, tf.cast(idx01, 'int32')), out_size)
    im10 = tf.reshape(tf.gather(imgs_flat, tf.cast(idx10, 'int32')), out_size)
    im11 = tf.reshape(tf.gather(imgs_flat, tf.cast(idx11, 'int32')), out_size)

    w00 = wt_x0 * wt_y0
    w01 = wt_x0 * wt_y1
    w10 = wt_x1 * wt_y0
    w11 = wt_x1 * wt_y1

    output = tf.add_n([
        w00 * im00, w01 * im01,
        w10 * im10, w11 * im11
    ])
    return output
    """