import os
import sys
import cv2
import torch
import argparse
import multiprocessing
import numpy as np
from tqdm import tqdm
from skimage import io
import matplotlib.cm as cm
import torch.optim as optim
from torch.utils.data import DataLoader
import torch.nn.functional as F
from pathlib import Path
from tensorboardX import SummaryWriter
from dataset import MegaDepthDataset
from models.superpoint import SuperPoint
from models.superglue_patches import SuperGlue


#from models.monodepth2 import MonoDepth
from loss import nll_loss, photometric_loss, pose_error
from models.utils import make_matching_plot_fast
from ground_truth import get_ground_truth, get_patches_points, warp_patches, get_pixels_from_pts
from utils import * 

MAX_KPTS = 400
PATCH_SIZE = 10

def train(lr, num_epochs, save_every, pos_weight, neg_weight, train_dataloader, validation_dataloader, 
         load_model_path, max_iter, checkpoints_path, config, device, writer, 
         use_log_loss=False, only_val=False, debugging=False):
    print('Log loss ', use_log_loss)

    iter = 0
    superpoint = SuperPoint(config.get('superpoint', {})).to(device)
    #mono_depth = MonoDepth().to(device).eval()

    for param in superpoint.parameters():
        param.requires_grad = False

    superglue = SuperGlue(config.get('superglue', {})).to(device)
    optimizer = optim.Adam(superglue.parameters(), lr=lr)
    start_epoch = 1
    start_step = 0
    loss = None
    if(load_model_path == None):
        path = Path(__file__).parent
        path = path / 'models/weights/superglue.pth'
        superglue = load_model_weights(superglue, path)    
    else:
        superglue, start_epoch, start_step, optimizer_state, loss = load_model_weights(superglue, load_model_path, 
                                                                                recover_state=True,
                                                                                modules=['kenc', 'penc', 'gnn', 'final_proj'])
        print('starting from epoch ', start_epoch)
        print('starting from step ', start_step)
        optimizer.load_state_dict(optimizer_state)

    #training only kenc
    # for param in superglue.gnn.parameters():
    #     param.requires_grad = False
    # for param in superglue.final_proj.parameters():
    #     param.requires_grad = False

    for epoch_idx in range(start_epoch, num_epochs + 1):
        train_size = min(max_iter, len(train_dataloader))
        train_pbar = tqdm(enumerate(train_dataloader), total=train_size)
        if(validation_dataloader != None):
            val_size = min(max_iter, len(validation_dataloader))
            val_pbar = tqdm(enumerate(validation_dataloader), total=val_size)
        training_losses = []
        validation_losses = []
                
        if(debugging):
            debug_folder_name = 'gdrive/My Drive/Doutorado/debug_dir/wpos'+str(pos_weight)+'_wneg'+str(neg_weight)+'_lr'+str(lr)+'_log'+str(use_log_loss)
            os.makedirs(debug_folder_name, exist_ok=True)
        
        if(not only_val):
            print('\n')
            print('='*20)
            print('Training...')
            if(epoch_idx == start_epoch and start_step > max_iter):
                continue
            
            for batch_idx, batch in train_pbar:
                if(batch_idx >= max_iter):
                    break
                if(batch_idx < start_step and epoch_idx == start_epoch):
                    continue
                superglue.train()
                optimizer.zero_grad()
                img0 = batch['image1'].to(device)
                #disp0 = mono_depth(img0)
                img1 = batch['image2'].to(device)
                #disp1 = mono_depth(img1)
                img0_gray = batch['gray1'].to(device)
                img1_gray = batch['gray2'].to(device)
            
                kpts = {}
                sp1 = superpoint({'image': img0_gray})
                kpts = {**kpts, **{k+'0': v for k, v in sp1.items()}}
                sp2 = superpoint({'image': img1_gray})
                kpts = {**kpts, **{k+'1': v for k, v in sp2.items()}}

                data = {'image0': img0_gray, 'image1': img1_gray}
                data = {**data, **kpts}
                #d0 = torch.unsqueeze(batch['depth1'], 1).to(device)
                #d1 = torch.unsqueeze(batch['depth2'], 1).to(device)
                data = pad_data(data, config['superpoint']['max_keypoints'], 
                                img0_gray.shape, device)
                # d0 = get_kpts_depths(data['keypoints0'], batch['depth1'], device)
                # d1 = get_kpts_depths(data['keypoints1'], batch['depth2'], device)

                # disps = {'disp0': d0, 'disp1': d1}
                # data = {**data, **disps}

                gt_matches = get_ground_truth(data['keypoints0'], 
                                             data['keypoints1'], 
                                             batch, device)

                patches_pts0 = get_patches_points(data['keypoints0'])
                patches_pts1 = get_patches_points(data['keypoints1'])                                     
                
                # patches_pts0, pts0_1 = warp_patches(patches_pts0, batch['depth1'], batch['intrinsics1'], batch['pose1'], 
                #                             batch['bbox1'], batch['depth2'], batch['intrinsics2'], 
                #                             batch['pose2'], batch['bbox2'])
                img0_pad = F.pad(img0, (PATCH_SIZE//2, PATCH_SIZE//2, PATCH_SIZE//2, PATCH_SIZE//2), "reflect")
                img1_pad = F.pad(img1, (PATCH_SIZE//2, PATCH_SIZE//2, PATCH_SIZE//2, PATCH_SIZE//2), "reflect")

                patches0 = get_pixels_from_pts(img0_pad, patches_pts0, output_shape=(patches_pts0.shape[0], patches_pts0.shape[1], PATCH_SIZE, PATCH_SIZE, 3))
                patches1 = get_pixels_from_pts(img1_pad, patches_pts1, output_shape=(patches_pts1.shape[0], patches_pts1.shape[1], PATCH_SIZE, PATCH_SIZE, 3))

                patches = {'patches0': patches0.to(device), 'patches1': patches1.to(device)}
                data = {**data, **patches}
                
                #pixels0_1 = get_pixels_from_pts(img1, pts0_1, output_shape=(pts0_1.shape[0], pts0_1.shape[1], PATCH_SIZE, PATCH_SIZE, 3))
                
                # pes = []
                # for i in range(pixels0.shape[0]):
                #     pes += [photometric_loss(pixels0[i,j], pixels0_1[i,j]) for j in range(pixels0[i].shape[0])]
                # pes = torch.Tensor(pes)
                
                # img0_cpu = img0.cpu().numpy()[0]
                # img0_cpu = np.moveaxis(img0_cpu, 0, -1)
                # img0_cpu = np.ascontiguousarray(img0_cpu*255, dtype=np.uint8)
                # img1_cpu = img1.cpu().numpy()[0]
                # img1_cpu = np.moveaxis(img1_cpu, 0, -1)
                # img1_cpu = np.ascontiguousarray(img1_cpu*255, dtype=np.uint8)
                
                # for patch in pts0[0]:
                #     img0_cpu = draw_patches(img0_cpu, [[patch[0], patch[-1]]], color=(0,0,0), thickness=2)
                # median_pe = torch.median(pes)
                # min_pe = torch.min(pes)
                # max_pe = torch.max(pes)

                # for patch, loss in zip(pts0_1[0], pes):
                #     color = (255*int(loss/max_pe ), 0, 0)
                #     if(loss < median_pe*0.5):
                #         color = (0,255,0)
                #     if(loss > median_pe*0.5 and loss < median_pe):
                #         color = (255,240,31)
                #     elif(loss > median_pe):
                #         color = (255,0,0)
                #     img1_cpu = draw_patches(img1_cpu, [[patch[0], patch[-1]]], color=color, thickness=2)

                # img0_cpu = draw_pts(img0_cpu, data['keypoints0'][0].cpu().numpy(), color=(255, 255, 255), radius=2, thickness=-1)
                # img1_cpu = draw_pts(img1_cpu, data['keypoints1'][0].cpu().numpy(), color=(255, 255, 255), radius=2, thickness=-1)

                # loss_img = np.concatenate((img0_cpu, img1_cpu), axis=1)
                # io.imshow(loss_img)
                # io.show()

                #for i in range(pixels0.shape[0]):
                #    pe = photometric_loss(pixels0[i], pixels0_1[i])
                
                #pts0 = torch.reshape(pts0, (pts0.shape[0], pts0.shape[1], PATCH_SIZE, PATCH_SIZE, 2))
                #pts0_1 = torch.reshape(pts0_1, (pts0_1.shape[0], pts0_1.shape[1], PATCH_SIZE, PATCH_SIZE, 2))
               
                # print('pts0_1 ', pts0_1.shape)
                # print('pts0 after ', pts0.shape)
                # for patch in pts0[0]:
                #     img0_cpu = draw_pts(img0_cpu, patch.cpu().numpy())

                # for patch in pts0_1[0]:
                #     img1_cpu = draw_pts(img1_cpu, patch.cpu().numpy())

                # io.imshow(img0_cpu)
                # io.show()

                # io.imshow(img1_cpu)
                # io.show()
                             
                if(gt_matches == None):
                    continue
                #Forward
                matches = superglue(data)
                #LOSS
                loss = nll_loss(matches, gt_matches, pos_weight=pos_weight, 
                                neg_weight=neg_weight, log_prob=use_log_loss)
                if(loss != None):
                    loss.backward()
                    optimizer.step()
                    current_loss = loss.item()
                    training_losses.append(current_loss)
                    train_pbar.set_postfix(loss=('%.4f' % np.mean(training_losses)))
                    
                    if(batch_idx%save_every == 0):
                        output_name = f'model_{epoch_idx}_{batch_idx}'
                        save_model(os.path.join(checkpoints_path, output_name+".pth"), 
                                superglue, optimizer, batch_idx, epoch_idx, loss)
                        title = 'Training_loss_iterations '
                        writer.add_scalar(title, np.mean(training_losses), iter)
                        writer.flush()
                    
                    if(debugging):
                        max0, max1 = matches[:, :-1, :-1].max(2), matches[:, :-1, :-1].max(1)
                        indices0, indices1 = max0.indices, max1.indices
                        mutual0 = arange_like(indices0, 1)[None] == indices1.gather(1, indices0)
                        mutual1 = arange_like(indices1, 1)[None] == indices0.gather(1, indices1)
                        zero = matches.new_tensor(0)
                        mscores0 = torch.where(mutual0, max0.values.exp(), zero)
                        mscores1 = torch.where(mutual1, mscores0.gather(1, indices1), zero)
                        valid0 = mutual0 & (mscores0 > config['superglue']['match_threshold'])
                        valid1 = mutual1 & valid0.gather(1, indices1)
                        indices0 = torch.where(valid0, indices0, indices0.new_tensor(-1))
                        indices1 = torch.where(valid1, indices1, indices1.new_tensor(-1))
                        gt_matches = ohe_to_le(gt_matches)

                        for i in range(gt_matches.size(0)):
                            if(batch_idx == 0):
                                k1 = data['keypoints0'][i].cpu().numpy()
                                k2 = data['keypoints1'][i].cpu().numpy()
                                im0_cpu = img0.cpu().numpy()[i][0]
                                im1_cpu = img1.cpu().numpy()[i][0]

                                matches_plot, conf = indices0.detach().cpu().numpy()[i], mscores0.detach().cpu().numpy()[i]
                                valid = matches_plot > -1
                                mkpts0 = k1[valid]
                                mkpts1 = k2[matches_plot[valid]]
                                mconf = conf[valid]
                                color = cm.jet(mconf)

                                text = ['SuperGlue',
                                        'Keypoints: {}:{}'.format(len(data['keypoints0'][i]), len(data['keypoints1'][i])),
                                        'Matches: {}'.format(len(mkpts0)),
                                        'Loss: {}'.format(loss.item())]
                                
                                make_matching_plot_fast(im0_cpu*255, im1_cpu*255, k1, k2,
                                                        mkpts0, mkpts1, color, text, 
                                                        path=os.path.join(debug_folder_name, 'match_'+str(epoch_idx)+'_'+str(batch_idx)+'_'+str(i)+'.png'), show_keypoints=True)
                                #saving gt
                                if(epoch_idx == 1):
                                    gt = gt_matches[i].cpu().numpy()
                                    ids1 = [x for x in range(len(gt)) if (gt[x] < MAX_KPTS and gt[x] != -1)]
                                    ids2 = [x for x in gt if (x < MAX_KPTS and x != -1)]
                                    im0_cpu = cv2.cvtColor(im0_cpu*255, cv2.COLOR_GRAY2BGR)
                                    im1_cpu = cv2.cvtColor(im1_cpu*255, cv2.COLOR_GRAY2BGR)

                                    kpts_img1 = create_kpts_image(im0_cpu, k1, color=(255, 0, 0))
                                    kpts_img2 = create_kpts_image(im1_cpu, k2, color=(0, 0, 255))
                                    
                                    gt_img = np.concatenate((kpts_img1, kpts_img2), axis=1)
                                    #gt_img = cv2.cvtColor(gt_img, cv2.COLOR_GRAY2BGR)

                                    for (p1, p2) in zip(k1[ids1], k2[ids2]):
                                        gt_img = cv2.line(gt_img, (int(p1[0]), int(p1[1])), 
                                                                (int(p2[0]+kpts_img1.shape[0]), int(p2[1])), 
                                                                color=(0,255,0), lineType=cv2.LINE_AA)

                                    cv2.putText(gt_img, 'Matches: '+str(len(ids1)), (10,500), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,0,0), 2)
                                    io.imsave(os.path.join(debug_folder_name, 'gt_image_'+str(epoch_idx)+'_'+str(batch_idx)+'_'+str(i)+'.png'), np.uint8(gt_img))
                    iter+=1
                title = 'Training_loss '
                writer.add_scalar(title, np.mean(training_losses), epoch_idx)
            output_name = f'model_{epoch_idx}_{len(train_pbar)}'
            save_model(os.path.join(checkpoints_path, output_name+".pth"), 
                                superglue, optimizer, len(train_pbar), epoch_idx, loss)
        if(validation_dataloader != None):
            print('\n')
            print('='*20)
            print('Validation...')
            #Validation Loop
            superglue.eval()
            with torch.no_grad():
                for batch_idx, batch in val_pbar:
                    img0 = batch['image1'].to(device)
                    #disp0 = mono_depth(img0)
                    img1 = batch['image2'].to(device)
                    #disp1 = mono_depth(img1)
                    #d0 = torch.unsqueeze(batch['depth1'], 1).to(device)
                    #d1 = torch.unsqueeze(batch['depth2'], 1).to(device)
                    img0_gray = batch['gray1'].to(device)
                    img1_gray = batch['gray2'].to(device)
                    kpts = {}
                    sp1 = superpoint({'image': img0_gray})
                    kpts = {**kpts, **{k+'0': v for k, v in sp1.items()}}
                    sp2 = superpoint({'image': img1_gray})
                    kpts = {**kpts, **{k+'1': v for k, v in sp2.items()}}

                    data = {'image0': img0_gray, 'image1': img1_gray}
                    data = {**data, **kpts}
                    data = pad_data(data, config['superpoint']['max_keypoints'],
                                    img0_gray.shape, device)
                    #d0 = get_kpts_depths(data['keypoints0'], batch['depth1'], device)
                    #d1 = get_kpts_depths(data['keypoints1'], batch['depth2'], device)
                    #disps = {'disp0': d0, 'disp1': d1}
                    #data = {**data, **disps}
                    
                    gt_matches = get_ground_truth(data['keypoints0'], 
                                        data['keypoints1'], 
                                        batch, device)
                    patches_pts0 = get_patches_points(data['keypoints0'])

                    patches_pts1 = get_patches_points(data['keypoints1'])          
                    img0_pad = F.pad(img0, (PATCH_SIZE//2, PATCH_SIZE//2, PATCH_SIZE//2, PATCH_SIZE//2), "reflect")
                    img1_pad = F.pad(img1, (PATCH_SIZE//2, PATCH_SIZE//2, PATCH_SIZE//2, PATCH_SIZE//2), "reflect")

                    patches0 = get_pixels_from_pts(img0_pad, patches_pts0, output_shape=(patches_pts0.shape[0], patches_pts0.shape[1], PATCH_SIZE, PATCH_SIZE, 3))
                    patches1 = get_pixels_from_pts(img1_pad, patches_pts1, output_shape=(patches_pts1.shape[0], patches_pts1.shape[1], PATCH_SIZE, PATCH_SIZE, 3))     
                    patches = {'patches0': patches0.to(device), 'patches1': patches1.to(device)}
                    data = {**data, **patches}
                    
                    #Forward
                    matches = superglue(data)
                    #LOSS
                    loss = nll_loss(matches, gt_matches, pos_weight=pos_weight, 
                                    neg_weight=neg_weight, log_prob=use_log_loss)
                    current_loss = loss.item()
                    validation_losses.append(current_loss)
                    val_pbar.set_postfix(loss=('%.4f' % np.mean(validation_losses)))                                   
        
                    if(debugging):
                        max0, max1 = matches[:, :-1, :-1].max(2), matches[:, :-1, :-1].max(1)
                        indices0, indices1 = max0.indices, max1.indices
                        mutual0 = arange_like(indices0, 1)[None] == indices1.gather(1, indices0)
                        mutual1 = arange_like(indices1, 1)[None] == indices0.gather(1, indices1)
                        zero = matches.new_tensor(0)
                        mscores0 = torch.where(mutual0, max0.values.exp(), zero)
                        mscores1 = torch.where(mutual1, mscores0.gather(1, indices1), zero)
                        valid0 = mutual0 & (mscores0 > config['superglue']['match_threshold'])
                        valid1 = mutual1 & valid0.gather(1, indices1)
                        indices0 = torch.where(valid0, indices0, indices0.new_tensor(-1))
                        indices1 = torch.where(valid1, indices1, indices1.new_tensor(-1))
                        gt_matches = ohe_to_le(gt_matches)

                        for i in range(gt_matches.size(0)):
                            k1 = data['keypoints0'][i].cpu().numpy()
                            k2 = data['keypoints1'][i].cpu().numpy()
                            im0_cpu = img0.cpu().numpy()[i][0]
                            im1_cpu = img1.cpu().numpy()[i][0]

                            matches_plot, conf = indices0.detach().cpu().numpy()[i], mscores0.detach().cpu().numpy()[i]
                            valid = matches_plot > -1
                            mkpts0 = k1[valid]
                            mkpts1 = k2[matches_plot[valid]]
                            mconf = conf[valid]
                            color = cm.jet(mconf)

                            text = ['SuperGlue',
                                    'Keypoints: {}:{}'.format(len(data['keypoints0'][i]), len(data['keypoints1'][i])),
                                    'Matches: {}'.format(len(mkpts0)),
                                    'Loss: {}'.format(loss.item())]
                            
                            make_matching_plot_fast(im0_cpu*255, im1_cpu*255, k1, k2,
                                                    mkpts0, mkpts1, color, text, 
                                                    path=os.path.join(debug_folder_name, 'match_'+str(epoch_idx)+'_'+str(batch_idx)+'_'+str(i)+'.png'), show_keypoints=True)
                            
                            gt = gt_matches[i].cpu().numpy()
                            ids1 = [x for x in range(len(gt)) if (gt[x] < MAX_KPTS and gt[x] != -1)]
                            ids2 = [x for x in gt if (x < MAX_KPTS and x != -1)]
                            im0_cpu = cv2.cvtColor(im0_cpu*255, cv2.COLOR_GRAY2BGR)
                            im1_cpu = cv2.cvtColor(im1_cpu*255, cv2.COLOR_GRAY2BGR)

                            kpts_img1 = create_kpts_image(im0_cpu, k1, color=(255, 0, 0))
                            kpts_img2 = create_kpts_image(im1_cpu, k2, color=(0, 0, 255))
                            
                            gt_img = np.concatenate((kpts_img1, kpts_img2), axis=1)

                            for (p1, p2) in zip(k1[ids1], k2[ids2]):
                                gt_img = cv2.line(gt_img, (int(p1[0]), int(p1[1])), 
                                                        (int(p2[0]+kpts_img1.shape[0]), int(p2[1])), 
                                                        color=(0,255,0), lineType=cv2.LINE_AA)
                            print('saving to ', debug_folder_name)
                            cv2.putText(gt_img, 'Matches: '+str(len(ids1)), (10,500), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,0,0), 2)
                            io.imsave(os.path.join(debug_folder_name, 'gt_image_'+str(epoch_idx)+'_'+str(batch_idx)+'_'+str(i)+'.png'), np.uint8(gt_img))
                
            title = 'Validation_loss '
            writer.add_scalar(title, np.mean(validation_losses), epoch_idx)
            writer.flush()       
        
        if(not only_val):
            output_name = f'model_{epoch_idx}'
            save_model(os.path.join(checkpoints_path, output_name+".pth"), 
                    superglue, optimizer, len(train_dataloader), epoch_idx, loss)


def main(lr, batch_size, num_epochs, save_every, dataset_path, train_scenes_path, 
        load_model_path, valid_scenes_path, logs_dir, max_iter,
        checkpoints_path, save_dataset_path, load_dataset_from_file,
        pos_weight, neg_weight, use_log_loss, only_val, debugging):
        
    os.makedirs(logs_dir, exist_ok=True)
    os.makedirs(checkpoints_path, exist_ok=True)
    config = {'superpoint': {'nms_radius': 4,
                            'keypoint_threshold': 0.005,
                            'max_keypoints': MAX_KPTS},
              'superglue': {'weights': 'outdoor',
                            'sinkhorn_iterations': 30,
                            'match_threshold': 0.2}}

    scenes_info_path = os.path.join(dataset_path, 'scene_info')
    
    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda:0" if use_cuda else "cpu")                              

    random_seed = 35 #33
    #Seed
    torch.manual_seed(random_seed)
    if use_cuda:
        torch.cuda.manual_seed(random_seed)
    np.random.seed(random_seed)
    train_dset = MegaDepthDataset(scene_list_path=train_scenes_path,
                            scene_info_path=scenes_info_path,
                            base_path=dataset_path,
                            preprocessing='torch',
                            min_overlap_ratio=0.1,
                            max_overlap_ratio=0.7,
                            image_size=720,
                            save_dataset_path=os.path.join(save_dataset_path, "train_dset.pkl"),
                            load_from_file=load_dataset_from_file)

    val_dset = MegaDepthDataset(scene_list_path=valid_scenes_path,
                                scene_info_path=scenes_info_path,
                                base_path=dataset_path,
                                train=False,
                                preprocessing='torch',
                                min_overlap_ratio=0.1,
                                max_overlap_ratio=0.7,
                                image_size=720,
                                save_dataset_path=os.path.join(save_dataset_path, "valid_dset.pkl"),
                                load_from_file=load_dataset_from_file)

    train_dataloader = DataLoader(train_dset, batch_size=batch_size, num_workers=multiprocessing.cpu_count())
    train_dset.build_dataset()
    
    validation_dataloader = DataLoader(val_dset, batch_size=batch_size, num_workers=multiprocessing.cpu_count())
    val_dset.build_dataset()

    writer = SummaryWriter(logs_dir,
        comment= "_LR_"+ str(lr) + "_Batch_size_" + str(batch_size))
    
    train(lr, num_epochs, save_every, pos_weight, neg_weight, 
          train_dataloader, validation_dataloader, load_model_path,
          max_iter, checkpoints_path, config, device, writer, 
          use_log_loss, only_val, debugging)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("dataset_path", default=None, help="Path to dataset")
    parser.add_argument("train_scenes_path", default=None, help="Path to train scenes txt")
    parser.add_argument("valid_scenes_path", default=None, help="Path to valid scenes txt")
    parser.add_argument("--load_model_path", default=None, help="Path to load model")
    parser.add_argument("--logs_dir", default="logs/", help="Path to save logs")
    parser.add_argument("--max_iter", default=1000, type=int, help="Max training iterations")
    parser.add_argument("--checkpoints_path", default="models/", help="Path to save checkpoints")   
    parser.add_argument("--save_dataset_path", default="logs/", help="Path to save built dataset")   
    parser.add_argument("--load_dataset_from_file", action='store_true', help="True if we should load the dataset from a pickle file")   
    parser.add_argument("--learning_rate", default=0.0001, type=float, help="Learning Rate")
    parser.add_argument("--batch_size", default=32, type=int, help="Batch Size")
    parser.add_argument("--num_epochs", default=100, type=int, help="Path to logs")
    parser.add_argument("--save_every", default=200, type=int, help="Save model after this number of iterations")
    parser.add_argument("--pos_weight", default=0.5, type=float, help="Weight to compute loss in positive samples")
    parser.add_argument("--neg_weight", default=0.5, type=float, help="Weight to compute loss in negative samples")
    parser.add_argument("--use_log_loss", action='store_true')
    parser.add_argument("--debugging", action='store_true')
    parser.add_argument("--only_val", action='store_true')

    args = parser.parse_args()
    
    main(args.learning_rate, args.batch_size, args.num_epochs, args.save_every,
         args.dataset_path, args.train_scenes_path, args.load_model_path, 
         args.valid_scenes_path, args.logs_dir, args.max_iter, args.checkpoints_path, 
         args.save_dataset_path, args.load_dataset_from_file,
         args.pos_weight, args.neg_weight, args.use_log_loss, args.only_val, args.debugging)